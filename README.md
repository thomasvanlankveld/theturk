Springer
========

Realtime chess app.

ToDo
----

#### Host

- Claim springer.meteor.com
- Claim a github io page?

#### Initial

- Create chessboard (in-browser)
- Allow chessboard to be played
- Create boards collection
- Store board state in backend
- Update UI across browsers according to game changes

#### Messages

- Create a message board

#### Clock

- Implement chess clock

#### History

- Store game history

#### Awesomeness

- Add Jerry Springer picture

#### API

- Expose DDP API

#### Multiple users

- Add user accounts
- *Multiple boards?*
- *Create boards?*
- *Identify players?*
